#include<iostream>
#include<cmath>
#include<ctime>

using namespace std;

int main() {
    /* Receiving 2 numbers as input */
    int n,q;
    cin >> n >> q;
    /* Initializing a double pointer variable to store the user input as an array */

    int **seq = new int* [n];

    for(int i=0; i<n; i++) {
        int c;
        cin >> c;
        int *b = new int [c];
        for(int j=0; j<c; j++) {
            int e;
            cin >> e;
            b[j] = e;
        }
        *(seq+i) = b;
    }

    for(int i=0; i<q; i++) {
        int x, y;
        cin >> x >> y;
        cout << seq[x][y] << endl;
    }
    return 0;
}